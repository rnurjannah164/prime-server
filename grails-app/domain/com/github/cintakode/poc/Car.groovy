package com.github.cintakode.poc

class Car {
	String model
	String name
	Double price
	String status
	
    static constraints = {
    		price nullable: true
    }
    
    static mapping = {
    		status defaultValue: "'A'"
    }
}
