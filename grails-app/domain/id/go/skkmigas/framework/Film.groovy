package id.go.skkmigas.framework

class Film {
    String judul
    Integer tahunRilis
    
    //=============================================brsAktif=============================================//
    //  Atribut brsAktif menandakan apakah sebuah row aktif atau tidak                                  //
    //                                                                                                  //
    //  Jika bernilai true, maka berarti row itu merupakan data aktif dan akan dapat digunakan untuk    //
    //  seluruh proses pada aplikasi                                                                    //
    //                                                                                                  //
    //  Jika bernilai false, maka berarti row itu bukan merupakan data aktif dan tidak akan dapat       //
    //  digunakan untuk seluruh proses pada aplikasi                                                    //
    //                                                                                                  //
    //  Dengan kata lain, jika brsAktif bernilai false, data tersebut telah "dihapus" dari aplikasi     //
    //=============================================brsAktif=============================================//
    boolean brsAktif
    
    //=================================Penjelasan atribut untuk Audit Trail=================================//
    //  createdBy    menandakan user mana yang membuat objek Sutradara a.k.a yang menambahkan sebuah row ke //
    //              tabel Sutradara pertama kali                                                            //
    //                                                                                                      //
    //  dateCreated  menandakan kapan sebuah objek Sutradara a.k.a kapan sebuah row pada tabel Sutradara    //
    //              ditambahkan pertama kali                                                                //
    //              Nilai tidak perlu diset, karena akan di auto timestamp oleh grails                      //
    //                                                                                                      //
    //  updatedBy    menandakan user mana yang terakhir kali membuat perubahan pada  objek Sutradara a.k.a  //
    //              yang terakhir mengubah sebuah row terkait di tabel Sutradara                            //
    //                                                                                                      //
    //  lastUpdated  menandakan kapan terakhir kali terjadi perubahan pada  objek Sutradara a.k.a kapan     //
    //              terakhir kali sebuah row terkait di tabel Sutradara diupdate                            //
    //              Nilai tidak perlu diset, karena akan di auto timestamp oleh grails                      //
    //=================================Penjelasan atribut untuk Audit Trail=================================//
    String createdBy
    Date dateCreated
    String updatedBy
    Date lastUpdated
    
    //======================================Film Many-To-One Sutradara======================================//
    //  Film memiliki relasi Many-To-One dengan Sutradara, karena didefinisikan bahwa seorang Sutradara     //
    //  dapat menyutradarai banyak film, dan sebuah film hanya dapat disutradarai oleh seorang Sutradara    //
    //                                                                                                      //
    //  Untuk menggambarkan relasi tersebut, digunakan static belongsTo pada Domain Class Film              //
    //  dan static hasMany pada Domain Class Sutradara                                                      //
    //                                                                                                      //
    //  Line "static belongsTo = [sutradara:Sutradara]" berarti sebuah Object Film akan mempunyai atribut   //
    //  Object Sutradara dengan nama Atribut sutradara                                                      //
    //======================================Film Many-To-One Sutradara======================================//
    static belongsTo = [sutradara:Sutradara]

    //========================================static constraints========================================//
    //  static constraints adalah atribut yang memungkinkan kita untuk mengubah constraint kolom-kolom  //
    //  pada tabel yang direpresentasikan oleh Domain Class terkait                                     //
    //                                                                                                  //
    //  Sebagai contoh, kita bisa mengubah constraint sehingga sebuah kolom bisa bernilai null, harus   //
    //  bernilai unique, dll                                                                            //
    //========================================static constraints========================================//
    static constraints = {
        createdBy nullable: true
        updatedBy nullable: true
    }
}