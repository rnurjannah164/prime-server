package id.go.skkmigas.framework

class Sutradara {
    String nama
    String tempatLahir
    Date tanggalLahir
    String pathToFoto
    
    //=============================================brsAktif=============================================//
    //  Atribut brsAktif menandakan apakah sebuah row aktif atau tidak                                  //
    //                                                                                                  //
    //  Jika bernilai true, maka berarti row itu merupakan data aktif dan akan dapat digunakan untuk    //
    //  seluruh proses pada aplikasi                                                                    //
    //                                                                                                  //
    //  Jika bernilai false, maka berarti row itu bukan merupakan data aktif dan tidak akan dapat       //
    //  digunakan untuk seluruh proses pada aplikasi                                                    //
    //                                                                                                  //
    //  Dengan kata lain, jika brsAktif bernilai false, data tersebut telah "dihapus" dari aplikasi     //
    //=============================================brsAktif=============================================//
    boolean brsAktif
    
    //=================================Penjelasan atribut untuk Audit Trail=================================//
    //  createdBy    menandakan user mana yang membuat objek Sutradara a.k.a yang menambahkan sebuah row ke //
    //              tabel Sutradara pertama kali                                                            //
    //                                                                                                      //
    //  dateCreated  menandakan kapan sebuah objek Sutradara a.k.a kapan sebuah row pada tabel Sutradara    //
    //              ditambahkan pertama kali                                                                //
    //              Nilai tidak perlu diset, karena akan di auto timestamp oleh grails                      //
    //                                                                                                      //
    //  updatedBy    menandakan user mana yang terakhir kali membuat perubahan pada  objek Sutradara a.k.a  //
    //              yang terakhir mengubah sebuah row terkait di tabel Sutradara                            //
    //                                                                                                      //
    //  lastUpdated  menandakan kapan terakhir kali terjadi perubahan pada  objek Sutradara a.k.a kapan     //
    //              terakhir kali sebuah row terkait di tabel Sutradara diupdate                            //
    //              Nilai tidak perlu diset, karena akan di auto timestamp oleh grails                      //
    //=================================Penjelasan atribut untuk Audit Trail=================================//
    String createdBy
    Date dateCreated
    String updatedBy
    Date lastUpdated
    
    //======================================Sutradara One-To-Many Film======================================//
    //  Sutradara memiliki relasi One-To-Many dengan Film, karena didefinisikan bahwa seorang Sutradara     //
    //  dapat menyutradarai banyak film, dan sebuah film hanya dapat disutradarai oleh seorang Sutradara    //
    //                                                                                                      //
    //  Untuk menggambarkan relasi tersebut, digunakan static hasMany pada Domain Class Sutradara           //
    //  dan static belongsTo pada Domain Class Film                                                         //
    //                                                                                                      //
    //  Line "static hasMany = [film: Film]" berarti sebuah Object Sutradara akan mempunyai atribut sebuah  //
    //  array Film[] dengan nama Atribut film                                                               //
    //======================================Sutradara One-To-Many Film======================================//
    static hasMany = [film: Film]
    
    //============================================static mapping============================================//
    //  static mapping adalah atribut yang memungkinkan kita untuk mengubah konfigurasi mapping dari Domain //
    //  class ke tabel di database                                                                          //
    //                                                                                                      //
    //  Sebagai contoh, kita bisa mendefinisikan nama kolom id, mendefinisikan composite id, mendefinisikan //
    //  nama atribut yang bernilai sebuah fungsi, dll                                                       //
    //============================================static mapping============================================//
    static mapping = {
        //=============================Date Extract=============================//
        //  Pada contoh di bawah, kita mendefinisikan tiga buah atribut, yaitu  //
        //                                                                      //
        //  tgl merupakan extract elemen tanggal dari sebuah atribut Date       //
        //  bln merupakan extract elemen bulan dari sebuah atribut Date         //
        //  thn merupakan extract elemen tahun dari sebuah atribut Date         //
        //                                                                      //
        //  Syntax ini dapat berjalan untuk database h2 dan oracle              //
        //  Untuk jenis database lain, mungkin perlu penyesuaian syntax         //
        //=============================Date Extract=============================//
        tgl formula: 'EXTRACT(DAY FROM tanggalLahir)'
        bln formula: 'EXTRACT(MONTH FROM tanggalLahir)'
        thn formula: 'EXTRACT(YEAR FROM tanggalLahir)'
    }

    //========================================static constraints========================================//
    //  static constraints adalah atribut yang memungkinkan kita untuk mengubah constraint kolom-kolom  //
    //  pada tabel yang direpresentasikan oleh Domain Class terkait                                     //
    //                                                                                                  //
    //  Sebagai contoh, kita bisa mengubah constraint sehingga sebuah kolom bisa bernilai null, harus   //
    //  bernilai unique, dll                                                                            //
    //========================================static constraints========================================//
    static constraints = {
        pathToFoto nullable: true
        createdBy nullable: true
        updatedBy nullable: true
    }
}