package id.go.skkmigas.auth

class User {
    transient springSecurityService

    String username
    String password
    boolean isOperator
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired
    
    String nama
    
    static hasMany = [role:Role]

    static transients = ['springSecurityService']

    static constraints = {
        username blank: false, unique: true
        password blank: true, nullable: true
        nama nullable: true
    }

    static mapping = {
        password column: '`password`'
    }

    Set<Role> getAuthorities() {
        Role.withCriteria {
            user {
                idEq(this.id)
            }
        }
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        if(password!="")
        //password = springSecurityService.encodePassword(password)
        password=password.encodeAsMD5()
        else
        password = " " //string kosong untuk ldap
    }
}
