package id.go.skkmigas.auth

class AuthenticationToken {

    String tokenValue
    String username

    static mapping = { version false }
}
