package id.go.skkmigas.auth

class Role {

    String authority
    
    static hasMany = [user:User]
    static belongsTo = User

    static mapping = {
        cache true
    }

    static constraints = {
        authority blank: false, unique: true
    }
}
