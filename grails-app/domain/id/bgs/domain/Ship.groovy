package id.bgs.domain

import gorm.AuditStamp

@gorm.AuditStamp
class Ship {

    String shipName
    Integer dockingNumber
    Date lastMaintenanceDate
    
    static hasOne = [manifestNote:ManifestNote]
    
    static belongsTo = [shipType:ShipType]
    
    static constraints = {
        manifestNote nullable:true
    }
}
