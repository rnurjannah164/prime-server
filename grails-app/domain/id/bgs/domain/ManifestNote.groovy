package id.bgs.domain

import grails.util.GrailsUtil
import gorm.AuditStamp

@gorm.AuditStamp
class ManifestNote {

    String note
    Ship ship
    
    static constraints = {
        
        String env = GrailsUtil.getEnvironment()
        switch (env) {
            case "development":
                note size:1..5000
            break;
        }
        
    }
    
    static mapping = {
    		
    		println(GrailsUtil.getEnvironment())
    		
        String env = GrailsUtil.getEnvironment()
		
        switch (env) {
            case "production":
            case "test":
                note sqlType: 'text'
            break;
        }
        
    }
}
