package id.bgs.domain

import gorm.AuditStamp

@gorm.AuditStamp
class ShipType {

    String name
    
    static constraints = {
    }
}
