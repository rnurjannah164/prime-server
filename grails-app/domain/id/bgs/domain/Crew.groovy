package id.bgs.domain

import gorm.AuditStamp

@gorm.AuditStamp
class Crew {

    String name
    
    static belongsTo = [ship:Ship]
    
    static constraints = {
    }
}
