class UrlMappings {

    static mappings = {
        "/$controller/$id"(parseRequest:true){
            action = [GET:"show", DELETE: "delete", PUT: "update"]
        }

            "/$controller/"(parseRequest:true){
            action = [GET:"eagerList", POST: "save",DELETE: "delete"]
        }

            "/$controller/search"(parseRequest:true){
            action = [POST:"search"]
        }

            "/$controller/totalCount"(parseRequest:true){
            action = [GET:"totalCount"]
        }

            "/$controller/$action?"(parseRequest:true){
        }

        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')
    }
}
