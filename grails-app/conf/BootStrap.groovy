
import id.bgs.util.GenerateData
import id.go.skkmigas.auth.Requestmap
import id.go.skkmigas.auth.Role
import id.go.skkmigas.auth.User
import id.go.skkmigas.framework.Film
import id.go.skkmigas.framework.Sutradara
import java.text.SimpleDateFormat

import id.bgs.domain.ManifestNote
import id.bgs.domain.Ship

class BootStrap {

    def gen = new GenerateData()
    
    // def init = { servletContext -> 
    
    // }
    /*
    def init = { servletContext ->
    	    User user = new User(
            username: "admin",
            password: "admin",
            nama: "Administrator",
            isOperator:true
        )
        user.save(failOnError:true)
		
        User user2 = new User(
            username: "kkks",
            password: "kkks",
            nama: "kkks",
            isOperator:true
        )
        user2.save(failOnError:true)
        
        User user3 = new User(
            username: "sas",
            password: "sas",
            nama: "sas",
            isOperator:true
        )
        user3.save(failOnError:true)
        
        User user4 = new User(
            username: "poc",
            password: "",
            isOperator:false
        )
        user4.save(failOnError:true)

        Role role = new Role(
            authority: "ROLE_ADMIN"
        )
        role.addToUser(User.findByUsername("admin")).addToUser(User.findByUsername("kkks")).addToUser(User.findByUsername("sas")).addToUser(User.findByUsername("poc")).save(failOnError:true)

        Sutradara s1 = new Sutradara(
            nama:"Trilaksono Aribowo",
            tempatLahir:"Jakarta",
            tanggalLahir:new SimpleDateFormat("dd-MM-yyyy").parse("19-12-1993"),
            brsAktif:true,
            createdBy:"admin",
            pathToFoto:null
        )
        s1.save(failOnError:true)
        
        Sutradara s2 = new Sutradara(
            nama:"Lalala Sasasas",
            tempatLahir:"Jajaja",
            tanggalLahir:new SimpleDateFormat("dd-MM-yyyy").parse("19-04-1991"),
            brsAktif:true,
            createdBy:"admin",
            pathToFoto:null
        )
        s2.save(failOnError:true)
        
        Film f1 = new Film(
            judul:"sas",
            tahunRilis:2015,
            sutradara:s1,
            brsAktif:true,
            createdBy:"admin",
        )
        f1.save(failOnError:true)
        
        Film f2 = new Film(
            judul:"ri",
            tahunRilis:2015,
            sutradara:s1,
            brsAktif:true,
            createdBy:"admin",
        )
        f2.save(failOnError:true)
        
        Film f3 = new Film(
            judul:"nananana",
            tahunRilis:2014,
            sutradara:s2,
            brsAktif:true,
            createdBy:"admin",
        )
        f3.save(failOnError:true)
        
        gen.genType()
        gen.genShip()
        
        	new ManifestNote(note:'sndskjvbdsjk dsjfdsjfhcds',ship: Ship.findByShipName("LWSS Peterman")).save(failOnError:true, flush: true)
    
    }
	*/

     def init = { servletContext ->
            
            if(Requestmap.count()<1){
            for (String url in [
                '/',
                '/index',
                '/index.gsp',
                '/**/favicon.ico',
                '/**/style.css',
                '/**/partials/template/welcome.html',
                '/**/js/**',
                '/**/css/**',
                '/**/images/**',
                '/**/src/**',
                '/**/skins/**',
                '/**/lib/**',
                '/login/*',
                '/logout/*',
                '/admin/**'
                ]) {
                new Requestmap(url: url, configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save(failOnError:true)
            }
            new Requestmap(url: '/**', configAttribute: 'ROLE_ADMIN').save(failOnError:true)
        }
        
            //new ManifestNote(note:'sndskjvbdsjk dsjfdsjfhcds',ship: Ship.findByShipName("LWSS Peterman")).save(failOnError:true, flush: true)
    }

    def destroy = {
    }
}
