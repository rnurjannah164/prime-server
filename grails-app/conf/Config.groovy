
import org.apache.log4j.DailyRollingFileAppender
import org.apache.log4j.PatternLayout

// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination

// The ACCEPT header will not be used for content negotiation for user agents containing the following strings (defaults to the 4 major rendering engines)
grails.mime.disable.accept.header.userAgents = ['Gecko', 'WebKit', 'Presto', 'Trident']
grails.mime.types = [ // the first one is the default format
    all:           '*/*', // 'all' maps to '*' or the first available format in withFormat
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
    form:          'application/x-www-form-urlencoded',
    html:          ['text/html','application/xhtml+xml'],
    js:            'text/javascript',
    json:          ['application/json', 'text/json'],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    hal:           ['application/hal+json','application/hal+xml'],
    xml:           ['text/xml', 'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*']

// Legacy setting for codec used to encode data with ${}
grails.views.default.codec = "html"

// The default scope for controllers. May be prototype, session or singleton.
// If unspecified, controllers are prototype scoped.
grails.controllers.defaultScope = 'singleton'

// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside ${}
                scriptlet = 'html' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        filteringCodecForContentType {
            //'text/html' = 'html'
        }
    }
}
 
grails.converters.encoding = "UTF-8"
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

environments {
    development {
        grails.logging.jul.usebridge = true
    }
    production {
        grails.logging.jul.usebridge = false
        // TODO: grails.serverURL = "http://www.changeme.com"
    }
}

// log4j configuration
log4j = {
    // Example of changing the log pattern for the default console appender:
    //
    //appenders {
    //    console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
    //}
    
    def logLayoutPattern = new PatternLayout("%d{yyyy-MM-dd HH:mm:ss.SSS} [%p] [%t] %l - %m%n")
    def logLayoutPattern2 = new PatternLayout("%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n")
    def pattern = new PatternLayout("%d{yyyy-MM-dd HH:mm:ss.SSS} %m%n")
		   
    appenders {
        file name:'auth', file:'logs/auth.log', layout: pattern
        console name:"stdout", layout: logLayoutPattern2
        appender new DailyRollingFileAppender(name:'file', file:"logs/app.log", datePattern:"'.'yyyy-MM-dd", layout:logLayoutPattern2)
    }
    root {
        error 'stdout','file'
    }

    error  'org.codehaus.groovy.grails.web.servlet',        // controllers
           'org.codehaus.groovy.grails.web.pages',          // GSP
           'org.codehaus.groovy.grails.web.sitemesh',       // layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping',        // URL mapping
           'org.codehaus.groovy.grails.commons',            // core / classloading
           'org.codehaus.groovy.grails.plugins',            // plugins
           'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate'
    
    info auth: ["id.go.skkmigas.rest.MyRestAuthenticationTokenJsonRenderer"], additivity: false
		   
    //debug  'org.springframework.security'
}

//url dbconsole
grails.dbconsole.enabled = true
grails.dbconsole.urlRoot = '/admin/dbconsole'

//Active directory setup, read the spring-security-ldap docs for more information
grails.plugin.springsecurity.ldap.active=true
grails.plugin.springsecurity.providerNames = ['ldapAuthProvider', 'dbAuthProvider']
grails.plugin.springsecurity.ldap.context.managerDn = 'CN=poc,OU=SKKMIGAS TEST,DC=devad,DC=com' //username
grails.plugin.springsecurity.ldap.context.managerPassword = 'skkmigas1234*'
grails.plugin.springsecurity.ldap.context.server = 'ldap://10.3.3.89:389'
grails.plugin.springsecurity.ldap.authorities.groupSearchBase = 'ou=SKKMIGAS TEST,dc=devad,dc=com'
grails.plugin.springsecurity.ldap.authorities.retrieveGroupRoles = false
grails.plugin.springsecurity.ldap.authorities.retrieveDatabaseRoles = true
grails.plugin.springsecurity.ldap.mapper.userDetailsClass = 'person'
grails.plugin.springsecurity.ldap.search.filter = '(sAMAccountName={0})'
grails.plugin.springsecurity.ldap.search.base = 'DC=bpmigas,DC=com'
grails.ldaploginConfig = "cn=[uid],dc=example,dc=org"
grails.plugin.springsecurity.ldap.search.attributes = ['sAMAccountName']

grails.plugin.springsecurity.successHandler.defaultTargetUrl = '/#/welcome'
grails.plugin.springsecurity.failureHandler.defaultTargetUrl = '/login/auth'

// Spring security config
grails.plugin.springsecurity.active=true
grails.plugin.springsecurity.userLookup.userDomainClassName = 'id.go.skkmigas.auth.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'id.go.skkmigas.auth.UserRole'
grails.plugin.springsecurity.authority.className = 'id.go.skkmigas.auth.Role'
grails.plugin.springsecurity.securityConfigType = 'Requestmap'
grails.plugin.springsecurity.requestMap.className = 'id.go.skkmigas.auth.Requestmap'

grails.plugin.springsecurity.rest.login.active=true
grails.plugin.springsecurity.rest.login.endpointUrl='/api/login'
grails.plugin.springsecurity.rest.login.failureStatusCode=401

grails.plugin.springsecurity.rest.login.useJsonCredentials = true
grails.plugin.springsecurity.rest.login.usernamePropertyName='username'
grails.plugin.springsecurity.rest.login.passwordPropertyName='password'

grails.plugin.springsecurity.rest.logout.endpointUrl='/api/logout'
grails.plugin.springsecurity.rest.token.validation.headerName='X-Auth-Token'

grails.plugin.springsecurity.rest.token.storage.useGorm = true
grails.plugin.springsecurity.rest.token.storage.gorm.tokenDomainClassName = 'id.go.skkmigas.auth.AuthenticationToken'

grails.plugin.springsecurity.rest.token.rendering.tokenPropertyName = 'token'
grails.plugin.springsecurity.rest.token.rendering.usernamePropertyName = 'username'
grails.plugin.springsecurity.rest.token.rendering.authoritiesPropertyName = 'roles'

grails.plugin.springsecurity.rest.token.validation.active=true
grails.plugin.springsecurity.rest.token.validation.endpointUrl='/api/validate'

grails.plugin.springsecurity.rest.token.generation.useSecureRandom = true
grails.plugin.springsecurity.rest.token.generation.useUUID = false


grails.plugin.springsecurity.rest.login.useRequestParamsCredentials = false

cors.headers = [
  'Access-Control-Allow-Headers': 'origin, authorization, accept, content-type, x-requested-with, x-auth-token'
]

grails.databinding.dateFormats = ["yyyy-MM-dd"]
grails.databinding.convertEmptyStringsToNull = false

grails.bootstrap.enabled = false
grails.alfresco.config = ['url':'http://10.3.3.66:8080','folder':'/FRAMEWORK','username':'admin','password':'skkmigas1234*'];

grails {
    plugin {
        audittrail {
            createdBy.field = "createdBy"
            editedBy.field = "editedBy"
            createdDate.field = "createdDate"
            editedDate.field = "editedDate"
        }
    }
}