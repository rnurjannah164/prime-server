import id.go.skkmigas.rest.MyRestAuthenticationTokenJsonRenderer
import id.go.skkmigas.auth.provider.DbAuthProvider
beans = {
    ldapUserDetailsMapper(id.go.skkmigas.auth.helper.MyUserDetailsContextMapper) {
		
    }
    userDetailsService(id.go.skkmigas.auth.helper.MyUserDetailsService){
		
    }
    dbAuthProvider(DbAuthProvider) {
        userDetailsService = ref('userDetailsService')
        springSecurityService = ref('springSecurityService')
    }
    restAuthenticationTokenJsonRenderer(MyRestAuthenticationTokenJsonRenderer) {
        usernamePropertyName = "${grailsApplication.config.grails.plugin.springsecurity.rest.token.rendering.usernamePropertyName}"
        tokenPropertyName = "${grailsApplication.config.grails.plugin.springsecurity.rest.token.rendering.tokenPropertyName}"
        authoritiesPropertyName = "${grailsApplication.config.grails.plugin.springsecurity.rest.token.rendering.authoritiesPropertyName}"
    }
}
