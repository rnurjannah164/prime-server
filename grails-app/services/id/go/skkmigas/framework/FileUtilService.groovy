package id.go.skkmigas.framework

import grails.transaction.Transactional

import java.util.Map

import org.apache.chemistry.opencmis.client.api.CmisObject
import org.apache.chemistry.opencmis.client.api.Document
import org.apache.chemistry.opencmis.client.api.Folder
import org.apache.chemistry.opencmis.client.api.ItemIterable
import org.apache.chemistry.opencmis.client.api.QueryResult
import org.apache.chemistry.opencmis.client.api.Repository
import org.apache.chemistry.opencmis.client.api.Session
import org.apache.chemistry.opencmis.client.api.SessionFactory
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl
import org.apache.chemistry.opencmis.commons.PropertyIds
import org.apache.chemistry.opencmis.commons.SessionParameter
import org.apache.chemistry.opencmis.commons.data.ContentStream
import org.apache.chemistry.opencmis.commons.enums.BindingType
import org.apache.chemistry.opencmis.commons.enums.VersioningState
import org.apache.chemistry.opencmis.commons.exceptions.CmisConnectionException
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException
import org.apache.chemistry.opencmis.commons.exceptions.CmisRuntimeException
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl
import org.springframework.web.multipart.MultipartFile

@Transactional
class FileUtilService {

    def grailsApplication
	
    String uploadDocumentAlfresco(MultipartFile file,String folderpath) throws Exception {
        def alfrescoParams=grailsApplication.config.grails.alfresco.config
		
        String generalPath=alfrescoParams.get("folder");;
        String urlAlfresco=alfrescoParams.get("url");

        // Default factory implementation of client runtime.
        SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
        Map<String, String> parameter = new HashMap<String, String>();

        // User credentials.
        parameter.put(SessionParameter.USER, alfrescoParams.get("username"));
        parameter.put(SessionParameter.PASSWORD, alfrescoParams.get("password"));

        // Connection settings.
        parameter.put(SessionParameter.ATOMPUB_URL,urlAlfresco+"/alfresco/service/cmis"); // URL to your CMIS server.
        // parameter.put(SessionParameter.REPOSITORY_ID, "myRepository"); //Only necessary if there is more than one repository.
        parameter.put(SessionParameter.BINDING_TYPE,BindingType.ATOMPUB.value());

        // Create session.
        Session session = null;
        // This supposes only one repository is available at the URL.
        Repository soleRepository = sessionFactory.getRepositories(parameter).get(0);
        session = soleRepository.createSession();
		
        String path;
        if(!folderpath.trim().equals("")){
            path=generalPath+"/"+folderpath;
        }else{
            path=generalPath;
        }
		
        Folder folder = (Folder) session.getObjectByPath(path);
        String filename=file.originalFilename.substring(0,file.originalFilename.lastIndexOf("."));
        String ext=file.originalFilename.substring(file.originalFilename.lastIndexOf("."),file.originalFilename.size());
        String mimetype=file.contentType;

        //cek nama dokumen if existing
        filename=getUniqueFileName(session,folder, path, filename, filename,ext,1)+ext;

        // properties
        // (minimal set: name and object type id)
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
        properties.put(PropertyIds.NAME, filename);

        // content
        InputStream stream = new ByteArrayInputStream(file.bytes);
        ContentStream contentStream = new ContentStreamImpl(file.originalFilename,BigInteger.valueOf(file.bytes.length), mimetype, stream);

        // create a major version
        Document newDoc = folder.createDocument(properties, contentStream,VersioningState.MAJOR);
        session.clear();
        
        return "/"+folderpath+"/"+filename;
    }

    String getUniqueFileName(Session session,Folder folder,String path,String original_filename,String filename,String ext,Integer count){
        filename=filename.replaceAll(" ", "_");
        filename=filename.replaceAll("-", "_");
        ItemIterable<QueryResult> results = session.query("SELECT cmis:name FROM cmis:document where cmis:name='"+filename+ext+"' and IN_FOLDER('"+folder.getId()+"')", false);
        if(results.getTotalNumItems()>0){
            filename=original_filename+"("+count+")";
            filename=getUniqueFileName(session, folder, path, original_filename,filename,ext,count+1);
        }

        return filename;
    }

    Object[] downloadAlfresco(String lokasi_file) throws Exception{
        def alfrescoParams=grailsApplication.config.grails.alfresco.config
        BufferedReader br = null;

        String generalPath=alfrescoParams.get("folder");
        String urlAlfresco=alfrescoParams.get("url");

        // Default factory implementation of client runtime.
        SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
        Map<String, String> parameter = new HashMap<String, String>();

        // User credentials.
        parameter.put(SessionParameter.USER, alfrescoParams.get("username"));
        parameter.put(SessionParameter.PASSWORD, alfrescoParams.get("password"));

        // Connection settings.
        parameter.put(SessionParameter.ATOMPUB_URL,urlAlfresco+"/alfresco/service/cmis"); // URL to your CMIS server.
        // parameter.put(SessionParameter.REPOSITORY_ID, "myRepository"); //Only necessary if there is more than one repository.
        parameter.put(SessionParameter.BINDING_TYPE,BindingType.ATOMPUB.value());

        // Create session.
        Session session = null;
        // This supposes only one repository is available at the URL.
        Repository soleRepository = sessionFactory.getRepositories(parameter).get(0);
        session = soleRepository.createSession();
        
        String lokasiFile = generalPath+lokasi_file;
        println lokasiFile

        Object[] result=new Object[3];
        Document document = (Document)session.getObjectByPath(lokasiFile);

        InputStream stream = document.getContentStream().getStream();
        String type = document.getContentStream().getMimeType();
        String filename = document.getName();

        result[0]=stream;
        result[1]=type;
        result[2]=filename;

        return result;
    }

    boolean deleteFileAlfresco(String lokasi_file) throws Exception{
        def alfrescoParams=grailsApplication.config.grails.alfresco.config
        String generalPath=alfrescoParams.get("folder");
        String urlAlfresco=alfrescoParams.get("url");

        // Default factory implementation of client runtime.
        SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
        Map<String, String> parameter = new HashMap<String, String>();

        // User credentials.
        parameter.put(SessionParameter.USER, alfrescoParams.get("username"));
        parameter.put(SessionParameter.PASSWORD, alfrescoParams.get("password"));

        // Connection settings.
        parameter.put(SessionParameter.ATOMPUB_URL,urlAlfresco+"/alfresco/service/cmis"); // URL to your CMIS server.
        // parameter.put(SessionParameter.REPOSITORY_ID, "myRepository"); //Only necessary if there is more than one repository.
        parameter.put(SessionParameter.BINDING_TYPE,BindingType.ATOMPUB.value());

        // Create session.
        Session session = null;
        // This supposes only one repository is available at the URL.
        Repository soleRepository = sessionFactory.getRepositories(parameter).get(0);
        session = soleRepository.createSession();

        Document document = (Document)session.getObjectByPath(lokasi_file);
        document.delete();

        return true;
    }


    boolean deleteFolderAlfresco(String lokasi_folder, boolean deleteSubFolder) throws Exception{
        def alfrescoParams=grailsApplication.config.grails.alfresco.config
        String generalPath=alfrescoParams.get("folder");
        String urlAlfresco=alfrescoParams.get("url");

        // Default factory implementation of client runtime.
        SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
        Map<String, String> parameter = new HashMap<String, String>();

        // User credentials.
        parameter.put(SessionParameter.USER, alfrescoParams.get("username"));
        parameter.put(SessionParameter.PASSWORD, alfrescoParams.get("password"));

        // Connection settings.
        parameter.put(SessionParameter.ATOMPUB_URL,urlAlfresco+"/alfresco/service/cmis"); // URL to your CMIS server.
        // parameter.put(SessionParameter.REPOSITORY_ID, "myRepository"); //Only necessary if there is more than one repository.
        parameter.put(SessionParameter.BINDING_TYPE,BindingType.ATOMPUB.value());

        // Create session.
        Session session = null;
        // This supposes only one repository is available at the URL.
        Repository soleRepository = sessionFactory.getRepositories(parameter).get(0);
        session = soleRepository.createSession();

        Folder folder = (Folder)session.getObjectByPath(generalPath + lokasi_folder);
        if(deleteSubFolder){
            folder.deleteTree(true, null, true);
        }else{
            folder.delete();
        }

        return true;
    }

    String createFolderAlfresco(String folderpath, String foldername) throws Exception {
        def alfrescoParams=grailsApplication.config.grails.alfresco.config
		
        String generalPath=alfrescoParams.get("folder");;
        String urlAlfresco=alfrescoParams.get("url");

        // Default factory implementation of client runtime.
        SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
        Map<String, String> parameter = new HashMap<String, String>();

        // User credentials.
        parameter.put(SessionParameter.USER, alfrescoParams.get("username"));
        parameter.put(SessionParameter.PASSWORD, alfrescoParams.get("password"));

        // Connection settings.
        parameter.put(SessionParameter.ATOMPUB_URL,urlAlfresco+"/alfresco/service/cmis"); // URL to your CMIS server.
        // parameter.put(SessionParameter.REPOSITORY_ID, "myRepository"); //Only necessary if there is more than one repository.
        parameter.put(SessionParameter.BINDING_TYPE,BindingType.ATOMPUB.value());

        // Create session.
        Session session = null;
        // This supposes only one repository is available at the URL.
        Repository soleRepository = sessionFactory.getRepositories(parameter).get(0);
        session = soleRepository.createSession();

        String path;
        if(!folderpath.trim().equals("")){
            path=generalPath+"/"+folderpath;
        }else{
            path=generalPath;
        }
        Folder folder = (Folder) session.getObjectByPath(path);
        //cek nama folder if existing
        Folder foldercheck=null;
		
        try{
            foldercheck = (Folder) session.getObjectByPath(path+"/"+foldername);
        }catch(Exception e){
			
        }
		
        String returnfolder="";
        if(foldercheck==null){
            Map<String, String> newFolderProps = new HashMap<String, String>();
            newFolderProps.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
            newFolderProps.put(PropertyIds.NAME, foldername);
            folder.createFolder(newFolderProps);

            if(!folderpath.trim().equals("")){
                returnfolder=folderpath+"/"+foldername;
            }else{
                returnfolder=foldername;
            }
            //returnfolder=path+"/"+foldername;
            //            returnfolder=foldername;
        }else{
            //returnfolder=path+"/"+foldercheck.getName();
            returnfolder=folderpath+"/"+foldercheck.getName();
            println "Folder name sudah ada"
        }

        session.clear();

        return returnfolder;
    }

    String changeFolderAlfresco(String folderpath, String oldfoldername, String newfoldername) throws Exception {
        def alfrescoParams=grailsApplication.config.grails.alfresco.config

        String generalPath=alfrescoParams.get("folder");;
        String urlAlfresco=alfrescoParams.get("url");

        // Default factory implementation of client runtime.
        SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
        Map<String, String> parameter = new HashMap<String, String>();

        // User credentials.
        parameter.put(SessionParameter.USER, alfrescoParams.get("username"));
        parameter.put(SessionParameter.PASSWORD, alfrescoParams.get("password"));

        // Connection settings.
        parameter.put(SessionParameter.ATOMPUB_URL,urlAlfresco+"/alfresco/service/cmis"); // URL to your CMIS server.
        // parameter.put(SessionParameter.REPOSITORY_ID, "myRepository"); //Only necessary if there is more than one repository.
        parameter.put(SessionParameter.BINDING_TYPE,BindingType.ATOMPUB.value());

        // Create session.
        Session session = null;
        // This supposes only one repository is available at the URL.
        Repository soleRepository = sessionFactory.getRepositories(parameter).get(0);
        session = soleRepository.createSession();

        String path;
        if(!folderpath.trim().equals("")){
            path=generalPath+"/"+folderpath;
        }else{
            path=generalPath;
        }
        Folder folder = (Folder) session.getObjectByPath(path+"/"+oldfoldername);

        Map<String, String> newFolderProps = new HashMap<String, String>();
        newFolderProps.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
        newFolderProps.put(PropertyIds.NAME, newfoldername);
        folder.updateProperties(newFolderProps);

        session.clear();

        return path+"/"+newfoldername;
    }
    
    boolean folderExists(String path) {
        def ret = false
        def alfrescoParams=grailsApplication.config.grails.alfresco.config

        String generalPath=alfrescoParams.get("folder");;
        String urlAlfresco=alfrescoParams.get("url");

        // Default factory implementation of client runtime.
        SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
        Map<String, String> parameter = new HashMap<String, String>();

        // User credentials.
        parameter.put(SessionParameter.USER, alfrescoParams.get("username"));
        parameter.put(SessionParameter.PASSWORD, alfrescoParams.get("password"));

        // Connection settings.
        parameter.put(SessionParameter.ATOMPUB_URL,urlAlfresco+"/alfresco/service/cmis"); // URL to your CMIS server.
        // parameter.put(SessionParameter.REPOSITORY_ID, "myRepository"); //Only necessary if there is more than one repository.
        parameter.put(SessionParameter.BINDING_TYPE,BindingType.ATOMPUB.value());

        // Create session.
        Session session = null;
        // This supposes only one repository is available at the URL.
        Repository soleRepository = sessionFactory.getRepositories(parameter).get(0);
        session = soleRepository.createSession();

        if(!path.trim().equals("")){
            path=generalPath+"/"+path;
        }else{
            path=generalPath;
        }
        
        try{
            println path
            Folder folder = (Folder) session.getObjectByPath(path);
            session.clear();
            ret = true
        } catch(CmisObjectNotFoundException e) {
            println "sas"
            session.clear();
            println ret
        }
        
        return ret
    }
}
