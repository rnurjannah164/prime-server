package com.github.cintakode.poc

import grails.converters.JSON

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = false)
class CarController {

    static allowedMethods = [save: "POST", update: "PUT", delete: ["POST", "DELETE"]]

    @Transactional
    def save() {
	    def instance = new Car(request.JSON)
		println (instance as String)
		instance.status = "S"
		def result = instance.save flush: true
		println (result as String)
		render instance as JSON
	}
	
	@Transactional
    def update() {
		/*
		println (params as String)
		def instance = Car.get(params.id)
		println (instance as String)
		instance.properties = params
		def result = instance.save flush: true
		println (result as String)
		render instance as JSON
		*/
		def req = request.JSON
		println (req as String)
		def instance = Car.get(req.id)
		println (instance as String)
		instance.properties = req
        instance.status = "U"
		def result = instance.save(flush:true, failOnError:true)
		println (result as String)
		render instance as JSON
	}
	
	@Transactional
    def delete() {
		def instance = Car.get(params.id)
		instance.status = "D"
		instance.save flush: true
		render instance as JSON
	}
}
