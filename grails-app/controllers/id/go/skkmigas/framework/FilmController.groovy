package id.go.skkmigas.framework

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import org.hibernate.criterion.CriteriaSpecification

class FilmController {
    static allowedMethods = [save: "POST", delete: "DELETE"]

    def index() {
        params.max = Math.min(max ?: 10, 100)
        respond Film.list(params), model:[filmInstanceCount: Film.count()]
    }
    
    def count() {
	def postParam=request.JSON
	def filmCount = Film.createCriteria().list() {
            if(postParam != null){
                if(postParam.first != null) firstResult(postParam.first)		    
		if(postParam.pageSize!=null)maxResults(postParam.pageSize)
                if(postParam.judul != null && postParam.judul != "") ilike('judul', '%'+postParam.judul+'%')
                if(postParam.tahunRilis != null && postParam.tahunRilis != 0) {  
                    eq('tahunRilis', postParam.tahunRilis)
                }
                if(postParam.sutradaraId != null && postParam.sutradaraId != 0) {
                    eq('sutradara.id', postParam.sutradaraId.toLong())
                }
	    }
	    resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            eq("brsAktif", true)                        
	}
	render filmCount.size()
    }
    
    def lazyList() {
        def postParam = request.JSON
        def filmList = Film.createCriteria().list() {                       
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            eq('brsAktif', true)
            
            createAlias('sutradara','sutradara',CriteriaSpecification.LEFT_JOIN)
            
            if(postParam != null){
		if(postParam.first != null) firstResult(postParam.first)		    
		if(postParam.pageSize!=null)maxResults(postParam.pageSize)
                if(postParam.judul != null && postParam.judul != "") ilike('judul', '%'+postParam.judul+'%')
                if(postParam.tahunRilis != null && postParam.tahunRilis != 0) {  
                    eq('tahunRilis', postParam.tahunRilis)
                }
                if(postParam.sutradaraId != null && postParam.sutradaraId != 0) {
                    eq('sutradara.id', postParam.sutradaraId.toLong())
                }
	    }
            
            projections {
                property('id','id')
                property('judul','judul')
                property('tahunRilis','tahunRilis')
                property('sutradara','sutradara')
                order('tahunRilis','desc')
                order('sutradara.nama','asc')
                order('judul','asc')
            }
        }
        
        render filmList as JSON
    }
    
    def eagerList() {
        def filmList = Film.createCriteria().list(params) {            
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            eq('brsAktif', true)
            
            createAlias('sutradara','sutradara',CriteriaSpecification.LEFT_JOIN)
            
            if(params.judul != null && params.judul != "") {
                eq("judul", params.judul)
            
            }
            if(params.tahunRilis != null && params.tahunRilis != ""){
                eq("tahunRilis", params.tahunRilis)
            }
            
            projections {
                property('id','id')
                property('judul','judul')
                property('tahunRilis','tahunRilis')
                property('sutradara','sutradara')
                order('tahunRilis','desc')
                order('sutradara.nama','asc')
                order('judul','asc')
            }
        }
        
        render filmList as JSON
    }
    
    def show() {
        def filmInstance = Film.createCriteria().list(params) {
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            idEq(params.id.toLong())
            eq('brsAktif', true)
            
            createAlias('sutradara','sutradara',CriteriaSpecification.LEFT_JOIN)
            
            projections {
                property('id','id')
                property('judul','judul')
                property('tahunRilis','tahunRilis')
                property('sutradara','sutradara')
            }
        }
        
        render filmInstance[0] as JSON
    }
    
    def create() {
        def createdBy = request.getHeader("username") != null ? request.getHeader("username") : "admin"
        
        println(createdBy)
        println(request.JSON)
        
        def instance = new Film(request.JSON)
        print request.JSON
        instance.createdBy = createdBy
        instance.brsAktif = true
        
        instance.save(flush: true, failOnError:true)
        
        if(!instance.hasErrors()) {
            render instance as JSON
        } else {
            print instance.errors as JSON
            print "======================================================================="
            print instance.errors.allErrors as JSON
        }
    }
    
    def delete() {
	def instance = Film.get(params.id.toLong())
        instance.brsAktif = false
	instance.save(flush: true, failOnError:true)
	render instance as JSON
    }
    
    def update() {
        def updatedBy = request.getHeader("username") != null ? request.getHeader("username") : "admin"
	def req = request.JSON
	def instance = Film.get(req.id)
	instance.properties = req
        instance.updatedBy = updatedBy
	instance.save(flush:true, failOnError:true)
	render instance as JSON
    }
    
    def batchCreate() {
        def createdBy = request.getHeader("username") != null ? request.getHeader("username") : "admin"
        JSONArray jsonArray = new JSONArray(request.JSON)
        def listFailure = []

        for(int s = 0; s < jsonArray.length(); s++){
            JSONObject row = new JSONObject(jsonArray.get(s))
            Film instance = new Film(row.get("data"))
            JSONObject rowData = new JSONObject(row.get("data"))
            instance.sutradara = Sutradara.findByNama(rowData.get("namaSutradara"))
            instance.createdBy = createdBy
            instance.brsAktif = true

            if (!instance.hasErrors() && instance.save(flush: true)) {

            } else {
                listFailure += row.get("nomor")
            }
        }
        
        render listFailure as JSON
    }
    
    def batchUpdate() {
        def updatedBy = request.getHeader("username") != null ? request.getHeader("username") : "admin"
        JSONArray jsonArray = new JSONArray(request.JSON)
        def listFailure = []

        for(int s = 0; s < jsonArray.length(); s++){
            JSONObject row = new JSONObject(jsonArray.get(s))
            JSONObject rowData = new JSONObject(row.get("data"))
            Film instance = Film.get(rowData.get("id"))
            instance.properties = row.get("data")
            instance.sutradara = Sutradara.findByNama(rowData.get("namaSutradara"))
            instance.updatedBy = updatedBy
            if (!instance.hasErrors() && instance.save(flush: true)) {

            } else {
                listFailure += row.get("nomor");
            }
        }

        render listFailure as JSON
    }
    
    def batchDelete() {
        def updatedBy = request.getHeader("username") != null ? request.getHeader("username") : "admin"
        JSONArray jsonArray = new JSONArray(request.JSON)
        def listFailure = []

        for(int s = 0; s < jsonArray.length(); s++){
            JSONObject row = new JSONObject(jsonArray.get(s))
            Film instance = Film.get(row.get("data"))
            instance.brsAktif = false
            instance.updatedBy = updatedBy
            if (!instance.hasErrors() && instance.save(flush: true)) {

            } else {
                listFailure += row.get("nomor");
            }
        }
		
        render listFailure as JSON
    }
}
