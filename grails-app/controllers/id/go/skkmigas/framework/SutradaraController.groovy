package id.go.skkmigas.framework

import grails.converters.JSON
import java.text.SimpleDateFormat
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest

class SutradaraController {
    
    def fileUtilService
    
    static allowedMethods = [save: "POST", delete: "DELETE"]

    def index(Integer max) { 
        params.max = Math.min(max ?: 10, 100)
        respond Sutradara.list(params), model:[sutradaraInstanceCount: Sutradara.count()]
    }
    
    def eagerList() {
        def sutradaraList = Sutradara.createCriteria().list(params) {
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            eq('brsAktif', true)
            
            if(params.nama != null && params.nama != '') {
                ilike('nama', '%' + params.nama + '%')
            }
            
            if(params.tempatLahir != null && params.tempatLahir != '') {
                ilike('tempatLahir', '%' + params.tempatLahir + '%')
            }
            
            if(params.tanggalLahir != null && params.tanggalLahir != '') {
                eq('tanggalLahir', new SimpleDateFormat('dd-MM-yyyy').parse(params.tanggalLahir))
            }
            
            projections {
                property('id','id')
                property('nama','nama')
                property('tempatLahir','tempatLahir')
                property('tanggalLahir','tanggalLahir')
                property('pathToFoto','pathToFoto')
                order('nama','asc')
            }
        }
        
        render sutradaraList as JSON
    }
    
    def show() {
        def sutradaraInstance = Sutradara.createCriteria().list(params) {
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            idEq(params.id.toLong())
            eq('brsAktif', true)                        
            
            projections {
                property('id','id')
                property('nama','nama')
                property('tempatLahir','tempatLahir')
                property('tanggalLahir','tanggalLahir')
                property('pathToFoto','pathToFoto')
            }
        }
        
        render sutradaraInstance[0] as JSON
    }
    
    def create() {
        def createdBy = request.getHeader("username") != null ? request.getHeader("username") : "admin"
        
        println(createdBy)
        println(request.JSON)
        
        def instance = new Sutradara(request.JSON)
        //instance.createdBy = createdBy
        instance.brsAktif = true
        
        instance.save(flush: true, failOnError:true)
        
        if(!instance.hasErrors()) {
            render instance as JSON
        } else {
            print instance.errors as JSON
            print "======================================================================="
            print instance.errors.allErrors as JSON
        }
    }
    
    def delete() {
	def instance = Sutradara.get(params.id.toLong())
        instance.brsAktif = false
	instance.save(flush: true, failOnError:true)
	render instance as JSON
    }
    
    def update() {
        def updatedBy = request.getHeader("username") != null ? request.getHeader("username") : "admin"
	def req = request.JSON
	def instance = Sutradara.get(req.id)
	instance.properties = req
        instance.updatedBy = updatedBy
	instance.save(flush:true, failOnError:true)
	render instance as JSON
    }
    
    def getFolderSutradara() {
        def sutradaraId = params.sutradaraId
        def sutradara = Sutradara.get(sutradaraId)
        def response = fileUtilService.createFolderAlfresco("", sutradara.nama)
        
        render response
    }
    
    def uploadProfPic() {
        def folder = params.folder
        if (request instanceof MultipartHttpServletRequest){
            def filename = URLDecoder.decode(request.getFileNames()[0], "UTF-8")
                
            MultipartFile file = request.getFile(filename)
			
            // Store file to Alfresco
            def response = fileUtilService.uploadDocumentAlfresco(file, folder)
            if (response != null) {
                def createdBy = request.getHeader("username") != null ? request.getHeader("username") : "admin"
                
                def instance = Sutradara.get(params.sutradaraId)
                instance.pathToFoto = response
                instance.updatedBy = createdBy
        
                instance.save(flush: true, failOnError:true)
        
                if(!instance.hasErrors()) {
                    render instance as JSON
                } else {
                    render instance.errors.allErrors as JSON
                }
            }
        }
    }
    
    def downloadProfPic() {
        Object[] result = fileUtilService.downloadAlfresco(params.filePath)
        if (result[0] != null) {
            response.status = 200 // OK
            response.contentType = result[1]
            response.setHeader("Content-disposition", "attachment; filename=\""+result[2]+"\"")
            response.outputStream << result[0]
        }
        
        render response
    }
}
