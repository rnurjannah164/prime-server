package id.bgs

import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest

class DokumenController {

    def index() { }
    
        
    def upload(){
        def folder = params.folder
        if (request instanceof MultipartHttpServletRequest){
            def filename = URLDecoder.decode(request.getFileNames()[0], "UTF-8")
                
            MultipartFile file = request.getFile(filename)
			
            // Store file to Alfresco
            def response = fileUtilService.uploadDocumentAlfresco(file, folder)
            
            render response
        }
    }
}
