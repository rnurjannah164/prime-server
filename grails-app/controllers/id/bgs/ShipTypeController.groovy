package id.bgs

import grails.converters.JSON
import id.bgs.domain.ShipType
import org.hibernate.criterion.CriteriaSpecification

class ShipTypeController {

    def index() { }
    
    def getList(){
        def instance = ShipType.createCriteria().list(){
            
        }
        
        render instance as JSON
    }
    
    def create() {
        def instance = new ShipType(request.JSON)
        
        instance.save(flush: true, failOnError:true)
        //print instance
        if(!instance.hasErrors()) {
            render instance as JSON
        } else {
            print instance.errors as JSON
            print "======================================================================="
            print instance.errors.allErrors as JSON
        }
    }
    
    def update() {
        def req = request.JSON
        def instance = ShipType.get(req.id)
            //def status = Status.get(instance.status.id)

        //instance.status = status
        instance.properties = req
        instance.save(failOnError: true)

        if (!instance.hasErrors()) {
            render instance as JSON
        } else {
            print instance.errors as JSON
            print "======================================================================="
            print instance.errors.allErrors as JSON
        }
    }
    
    def delete() {
	def instance = ShipType.get(params.id.toLong())
	instance.delete(flush: true)
	render instance as JSON
    }
    
    def getOne(){
        def instance = ShipType.withCriteria(uniqueResult: true) {
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            
            eq("id", params.id.toLong())
        }
        
        render instance as JSON
    }
    
}
