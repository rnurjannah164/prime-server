package id.bgs

import grails.converters.JSON
import id.bgs.domain.Ship
import org.hibernate.criterion.CriteriaSpecification

class ShipController {

    def index() { }
    
    def getList(){
        def ships = Ship.createCriteria().list(){            
        }
        
        render ships as JSON
    }
    
    def create() {
        def instance = new Ship(request.JSON)
        
        instance.save(flush: true, failOnError:true)
        //print instance
        if(!instance.hasErrors()) {
            render instance as JSON
        } else {
            print instance.errors as JSON
            print "======================================================================="
            print instance.errors.allErrors as JSON
        }
    }
    
    def update() {
        def req = request.JSON
        def instance = Ship.get(req.id)
            //def status = Status.get(instance.status.id)

        //instance.status = status
        instance.properties = req
        instance.lastMaintenanceDate = new Date()
        instance.save(failOnError: true)

        if (!instance.hasErrors()) {
            render instance as JSON
        } else {
            print instance.errors as JSON
            print "======================================================================="
            print instance.errors.allErrors as JSON
        }
    }
    
    def delete() {
	def instance = Ship.get(params.id.toLong())
	instance.delete(flush: true)
	render instance as JSON
    }
    
    def getOne(){
        def ship = Ship.withCriteria(uniqueResult: true) {
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            createAlias('manifestNote', 'manifestNote', CriteriaSpecification.LEFT_JOIN)
            createAlias('shipType', 'shipType', CriteriaSpecification.LEFT_JOIN)
            
            eq("id", params.id.toLong())
        }
        
        render ship as JSON
    }
    
}
