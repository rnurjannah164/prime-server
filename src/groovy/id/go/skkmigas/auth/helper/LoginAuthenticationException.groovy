package id.go.skkmigas.auth.helper

import org.springframework.security.core.AuthenticationException;

class LoginAuthenticationException extends AuthenticationException {
    String errCode;

    LoginAuthenticationException(String code) {
        super(code)
        this.errCode = code;
    }
}
