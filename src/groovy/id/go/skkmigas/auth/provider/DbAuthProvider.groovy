package id.go.skkmigas.auth.provider

import grails.util.Holders
import id.go.skkmigas.auth.User

import javax.naming.AuthenticationException
import javax.naming.Context
import javax.naming.NamingException
import javax.naming.directory.DirContext
import javax.naming.directory.InitialDirContext

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.GrantedAuthorityImpl

import id.go.skkmigas.auth.helper.LoginAuthenticationException
import id.go.skkmigas.auth.helper.MyUserDetails

class DbAuthProvider extends DaoAuthenticationProvider {
    def grailsApplication
    //	def passwordEncoder
    def springSecurityService
	
    @Override
    Authentication authenticate(Authentication authentication) {
        User user = User.findByUsernameAndEnabled(authentication.principal, true)
		
        if (!user){
            throw new LoginAuthenticationException("com.btpn.skepia.errors.login.not_found");
        }
		
        try{
            return dbAuthentication(authentication)
        }catch(Exception e){
            e.printStackTrace();
            throw new LoginAuthenticationException("id.go.skkmigas.errors.login.wrong");
        }	 
    }
	
    Authentication dbAuthentication(Authentication authentication) throws Exception {
        def username = (String)authentication.principal
        def password = (String)authentication.credentials

        def encodedPassword = password.encodeAsMD5() 
		
        User user = User.findByUsernameAndPasswordAndEnabled(username, encodedPassword , true)
		
        if (user != null){			
            return getToken(user);
        }else{
            return null;
        }
			
			
    }

    Authentication getToken(User user){
        Collection<GrantedAuthority> authorities = user.getAuthorities().collect { new GrantedAuthorityImpl(it.authority) };
        MyUserDetails userDetails = new MyUserDetails(user.username, user.password, true, true, true,  true, authorities, user.id, user.nama);
		
        return new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities())
    }
}
