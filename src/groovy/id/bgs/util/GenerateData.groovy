/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package id.bgs.util

import id.bgs.domain.Crew
import id.bgs.domain.ManifestNote
import id.bgs.domain.Ship
import id.bgs.domain.ShipType

/**
 *
 * @author irvannoviansyah
 */
class GenerateData {
	def string_ship_type = [
            "Corveratte",
            "Frigate",
            "Cruiser",
            "Carrier",
            "Dreadnought",
            "Destroyer",
            "Battleship"
        ]
    
    def genType (){
        for(String st: string_ship_type){
            new ShipType(
                name:st
            ).save(failOnError:true, flush: true)
        }
    }
    
    def genShip(){
        def ship1 = new Ship(
            shipName:'LWSS Peterman',
            dockingNumber:23,
            shipType: ShipType.findByName('Corveratte'),
            lastMaintenanceDate:new Date()
        ).save(failOnError:true, flush: true)
        //new ManifestNote(note:'sndskjvbdsjk dsjfdsjfhcds',ship:ship1).save(failOnError:true, flush: true)
        new Crew(ship:ship1,name:"Slavomir Lotta").save(failOnError:true, flush: true)
        new Crew(ship:ship1,name:"Varg Lina").save(failOnError:true, flush: true)
        
        def ship2 = new Ship(
            shipName:'Bellisarius',
            dockingNumber:98,
            shipType: ShipType.findByName('Frigate'),
            lastMaintenanceDate:new Date()
        ).save(failOnError:true, flush: true)
        //new ManifestNote(note:'asd sadsad sadsad sadasdas sadsada',ship:ship2).save(failOnError:true, flush: true)
        new Crew(ship:ship2,name:"Trish Steiner").save(failOnError:true, flush: true)
        new Crew(ship:ship2,name:"Wanetta Bledsoe").save(failOnError:true, flush: true)
        
        def ship3 = new Ship(
            shipName:'Celerity',
            dockingNumber:83,
            shipType: ShipType.findByName('Carrier'),
            lastMaintenanceDate:new Date()
        ).save(failOnError:true, flush: true)
        new Crew(ship:ship3,name:"Dina Shaffer").save(failOnError:true, flush: true)
    }
}

